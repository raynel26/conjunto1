//Funciones Auxiliares:
template <class T>
bool Conjunto<T>::esHoja(Nodo *nodo) {
    bool res = ( nodo->der == nullptr && nodo->izq == nullptr);
    return res;
}

template <class T>
bool Conjunto<T>::padreDeUnHijo(Nodo* nodo, bool &hijo) {
    bool res1 = (nodo->der != nullptr && nodo->izq == nullptr);
    bool res2 = (nodo->der == nullptr && nodo->izq != nullptr);
    if (res1){
        hijo = true;
    }
    if (res2){
        hijo = false;
    }
    return res1 || res2;
}

template<class T>
Conjunto<T>::Nodo::Nodo(const T &v) : valor(v), der(nullptr), izq(nullptr){}


template <class T>
Conjunto<T>::Conjunto(): _raiz(nullptr), _cardinal(0), min(), max(){}

template <class T>
Conjunto<T>::~Conjunto() {
    if(_cardinal == 1){
        delete _raiz;
    }else{
        Nodo* n = _raiz;
        eliminartodos(n);
    }
}

template<class T>
void Conjunto<T>::eliminartodos(Nodo *n) {
    if(n != nullptr){
        eliminartodos(n->der);
        eliminartodos(n->izq);
        delete n;
    }

}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* p = _raiz;
    while (p != nullptr && p->valor != clave){
        if (clave > p->valor){
            p = p->der;
        }else{
            p = p->izq;
        }
    }
    return p != nullptr;
}


template <class T>
void Conjunto<T>::insertar(const T& clave){
    Nodo* p = _raiz;
    if(!pertenece(clave)){
         if (p == nullptr){             //si es vacio
             Nodo* r = new Nodo(Nodo(clave));
             _raiz = r;
             _cardinal++;
             max = clave;
             min = clave;
         }else{                          //si no es vacio
            Nodo* padreDenodo;
            bool comp;
            while (p != nullptr){           //Busco nodo donde entraria
                comp = (clave > p->valor);
                if (comp){
                    padreDenodo = p;
                    p = p->der;
                }else{
                    padreDenodo = p;
                    p = p->izq;
                }
            }
            p = new Nodo(Nodo(clave)); //conecto el padre a su hijo correspondiente
            if (comp){
                padreDenodo->der = p;
            } else{
                padreDenodo->izq = p;
            }
            _cardinal++;                    //Aumento el cardinal
            if (clave > max){               //Modifico el maximo y minimo si corresponde
                max = clave;
            }
            if (clave < min){
                min = clave;
            }
        }
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    Nodo* p = _raiz;
    Nodo* padreDenodo;                            // puntero al padre del nodo a eliminar
    bool NodoAeliminaresHijoDerecho = true;
    while(p != nullptr && p->valor != clave){               // Buscar elemento a borrar
        if(clave > p->valor){
            padreDenodo = p;
            p = p->der;
            NodoAeliminaresHijoDerecho = true;
        }else{
            padreDenodo = p;
            p = p->izq;
            NodoAeliminaresHijoDerecho = false;
        }
    }
    if(p != nullptr){  // si esta el elemento
         if(esHoja(p)){                                                                             // si es Hoja
             if(p == _raiz){
                 delete p;
                 _raiz = nullptr;
                 p = nullptr;
                 _cardinal--;
             }else{
                 delete p;
                 _cardinal--;
                 if (NodoAeliminaresHijoDerecho){
                     padreDenodo->der = nullptr;
                 }else{
                     padreDenodo->izq = nullptr;
                 }
                if (max = clave){
                    max = clave;
                }
                if (min = clave){
                    min = clave;
                }
             }
         }
         else{
             bool NodoHijo_De_Nodo_A_Eliminar_es_hijo_derecho;
             if (padreDeUnHijo(p, NodoHijo_De_Nodo_A_Eliminar_es_hijo_derecho)){           // si es padre de un hijo
                if (NodoHijo_De_Nodo_A_Eliminar_es_hijo_derecho) {  // padre de hijo derecho
                    if(p == _raiz){
                        _raiz = p->der;
                        delete p;
                        _cardinal--;
                    }else{
                        if (NodoAeliminaresHijoDerecho){
                            padreDenodo->der = p->der;
                            delete p;
                            p = nullptr;
                            _cardinal--;
                        }else{
                            padreDenodo->izq = p->der;
                            delete p;
                            p = nullptr;
                            _cardinal--;
                        }
                    }
                }else{                     // padre de hijo izquierdo
                    if(p == _raiz){
                        _raiz = p->izq;
                        delete p;
                        _cardinal--;
                    }else{
                        if (NodoAeliminaresHijoDerecho){
                            padreDenodo->der = p->izq;
                            delete p;
                            p = nullptr;
                            _cardinal--;
                        }else{
                            padreDenodo->izq = p->izq;
                            delete p;
                            p = nullptr;
                            _cardinal--;
                        }
                    }
                }
             }else{                                                                         // si es padre de dos Hijos
                Nodo* padreDepredecesor = p;
                Nodo* predecesor = p->izq;
                if (predecesor->der == nullptr){
                    p->valor = predecesor->valor;
                    padreDepredecesor->izq = predecesor->izq;
                    delete predecesor;
                    predecesor = nullptr;
                    _cardinal--;
                }else{
                    while (predecesor->der != nullptr){
                        padreDepredecesor = predecesor;
                        predecesor = predecesor->der;
                    }
                    p->valor = predecesor->valor;
                    padreDepredecesor->der = predecesor->izq;
                    delete predecesor;
                    predecesor = nullptr;
                    _cardinal--;
                }
             }
        }
    }
}
template<class T>
const T & Conjunto<T>::siguiente(const T &elem) {
    if (elem < _raiz->valor){  // si el valor esta en el subArbol izquierdo
        Nodo* q = _raiz;                          //padre del elemento a buscar
        Nodo* p = _raiz->izq;
        while (p != nullptr && p->valor != elem){ // Buscar elemento
            if (elem > p->valor){
                q = p;
                p = p->der;
            }else{
                q = p;
                p = p->izq;
            }
        }
        if (p->der != nullptr){
            return (p->der)->valor;
        }else{
            return q->valor;
        }

    }else{                      // si el valor esta en el subArbol derecho o es la raiz
        Nodo* p = _raiz;
        while (p != nullptr && p->valor != elem){ // Buscar elemento
            if (elem > p->valor){
                p = p->der;
            }else{
                p = p->izq;
            }
        }
        Nodo* n = p->der;                        //Buscar el sucesor inmediato
        while (n->izq != nullptr){
            n = n->izq;
        }
        return n->valor;
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    return min;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    return max;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

